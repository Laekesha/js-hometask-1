/**
 * 1. Напиши функцию convertCelsiusToFahrenheit,
 * которая принимает на вход число — количество градусов в шкале Цельсия и возвращает число — количество
 * градусов в шкале Фаренгейта. Для перевода градусов Цельсия в градусы Фаренгейта, воспользуйся формулой
 * f = (c × 9/5) + 32, где f — градусы Фаренгейта, c — градусы Цельсия.
 */
function convertCelsiusToFahrenheit (degrees) {
  return (degrees * 9/5) + 32;
}

/**
 * 2. Напиши функцию convertStringToNumber, которая принимает на вход строку, и, если строка приводится к числу,
 * то возвращает это число, иначе возвращает false.
 */
function convertStringToNumber (str) {  // https://stackoverflow.com/a/175787/10419822
  if (isNaN(str)) {
    return false;
  }
  return parseInt(str);
}

/**
 * 3. Напиши функцию getNaN, возвращающую NaN, который должен получаться из строки abc
 * с помощью бинарного или унарного оператора.
 */
function getNaN () { // https://medium.com/@jsnoob/javascript-unary-operators-simple-and-useful-f9b02dbdea28
  return +'abc';
}

/**
 * 4. Напиши функцию createGratitude, которая принимает имя пользователя и оценку — число от 1 до 5,
 * и возвращает строку: {Имя пользователя} оценил нас на {оценка} из 5. Спасибо, {Имя пользователя}!.
 * Если имя не задано, то писать Аноним. Если не задана оценка, то писать 0.
 */
function createGratitude (name, rating) { // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined
  if (name === undefined) {
    name = 'Аноним';
  }
  if (rating === undefined) {
    rating = 0;
  }
  return name + ' оценил вас на ' + rating + ' из 5. Спасибо, ' + name + '!';
}

/**
 * 5. Напиши функции checkA1, checkA2, checkA3, которые возвращают значение `a`,
 * если `a` не равен нулю и строку "Все плохо", если `a` равен 0.
 * Сделай это при помощи:
 *   Конструкции if-else.
 *   Тернарного оператора.
 *   Логического или (||).
 */
function checkA1 (a) {
  if (a !== 0) {
    return a;
  }
  else {
    return 'Всё плохо';
  }

}

function checkA2 (a) {
  return a !== 0 ? a : 'Всё плохо';

}

function checkA3 (a) {
  /*
      https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators
      https://developer.mozilla.org/en-US/docs/Glossary/falsy
      https://developer.mozilla.org/en-US/docs/Glossary/truthy
      */
  return a || 'Всё плохо';

}

/**
 * 6. Напиши функцию squaresSum, которая принимает на вход границы диапазона чисел (нижнюю - min, и верхнюю - max)
 * возвращает сумму квадратов всех чисел, входящих в диапазон. Например:
 *  (5,6) → (25 + 36) → 61
 *  (1,4) → (1 + 4 + 9 + 16) → 30
 */
function squaresSum (min, max) {
  let sum = 0;
  for (let i = min; i <= max; i++) {
    sum += i * i;
  }
  return sum;
}

module.exports = {
  convertCelsiusToFahrenheit,
  convertStringToNumber,
  getNaN,
  createGratitude,
  checkA1,
  checkA2,
  checkA3,
  squaresSum
};